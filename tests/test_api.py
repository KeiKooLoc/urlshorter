import unittest
import json
# Internal
from tests.data import TestData
from app import create_app, db


class APITestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('test', expired_checker=False)
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()
        self.api_host = '/api'

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_404(self):
        response = self.client.get('/wrong/url')
        self.assertEqual(response.status_code, 404)
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(json_response['message'],
                         'The requested URL was not found on the server. '
                         'If you entered the URL manually '
                         'please check your spelling and try again.')

    def test_short(self):
        endpoint = f'{self.api_host}/short'
        request_json = {'url': TestData.VALID_URL,
                        'life_span': TestData.VALID_LIFE_SPAM}
        response = self.client.post(endpoint, json=request_json)
        self._test_success_short(response)

    def test_optional_life_span(self):
        endpoint = f'{self.api_host}/short'
        request_json = {'url': TestData.VALID_URL}
        response = self.client.post(endpoint, json=request_json)
        self._test_success_short(response)

    def _test_success_short(self, response):
        self.assertEqual(response.status_code, 201)
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(json_response), 4)
        self.assertEqual(json_response['ok'], True)
        self.assertEqual(json_response['original'], TestData.VALID_URL)
        self.assertIsInstance(json_response['expired_in'], str)
        self.assertIsInstance(json_response['new'], str)
        redirect_response = self.client.get(json_response['new'])
        self.assertEqual(redirect_response.status_code, 302)

    def test_short_validation(self):
        endpoint = f'{self.api_host}/short'
        invalid_url_msg = 'Invalid link, check it and try again.'
        invalid_life_span_msg = 'life_span is out of range 1 - 360.'

        # Check invalid link and invalid life span.
        request_json = {'url': TestData.INVALID_URL,
                        'life_span': TestData.INVALID_LIFE_SPAM}
        response = self.client.post(endpoint, json=request_json)
        self.assertEqual(response.status_code, 400)
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(json_response), 1)
        self.assertEqual(len(json_response['message']), 2)
        self.assertEqual(json_response['message']['url'],
                         invalid_url_msg)
        self.assertEqual(json_response['message']['life_span'],
                         invalid_life_span_msg)

        # Check valid link and invalid life span.
        request_json = {'url': TestData.VALID_URL,
                        'life_span': TestData.INVALID_LIFE_SPAM}
        response = self.client.post(endpoint, json=request_json)
        self.assertEqual(response.status_code, 400)
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(json_response), 1)
        self.assertEqual(len(json_response['message']), 1)
        self.assertEqual(json_response['message']['life_span'],
                         invalid_life_span_msg)

        # Check invalid link and valid life span.
        request_json = {'url': TestData.INVALID_URL,
                        'life_span': TestData.VALID_LIFE_SPAM}
        response = self.client.post(endpoint, json=request_json)
        self.assertEqual(response.status_code, 400)
        json_response = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(json_response), 1)
        self.assertEqual(len(json_response['message']), 1)
        self.assertEqual(json_response['message']['url'], invalid_url_msg)


if __name__ == "__main__":
    unittest.main()
