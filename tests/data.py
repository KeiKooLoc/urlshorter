from dataclasses import dataclass


@dataclass
class TestData:
    """Holds testing data."""

    VALID_URL: str = (
        "https://www.amazon.com/dp/B07M82PNSX/ref=s9_acsd_hps_bw_c2_x_0_i"
        "?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-8"
        "&pf_rd_r=T0PS9P7CSSM4WWYPJ78J&pf_rd_t=101"
        "&pf_rd_p=c38ef39a-9096-4d71-8da4-f0ca7a21d4d8&pf_rd_i=283155"
    )
    INVALID_URL: str = 'http:// Invalid link example'
    VALID_LIFE_SPAM: int = 1
    INVALID_LIFE_SPAM: int = 500
