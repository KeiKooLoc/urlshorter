import re
import string as py_string
# External
from flask import request
# Typing
from typing import Tuple, Optional


class LinkHandler:

    PATTERN = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+'
        r'(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE
    )
    ERROR = 'Invalid link, check it and try again.'

    MIN_LIFE_SPAN = 1
    MAX_LIFE_SPAN = 360
    DEFAULT_LIFE_SPAN = 90

    @classmethod
    def validate_url(cls, value: str) -> Tuple[bool, str]:

        if not cls.PATTERN.match(value):
            return False, cls.ERROR

        if value.startswith(request.url_root):
            return False, 'Don\'t use links with website root host.'

        return True, ''

    @classmethod
    def validate_life_span(cls, value: int) -> Tuple[bool, str]:

        if value not in range(cls.MIN_LIFE_SPAN, cls.MAX_LIFE_SPAN):
            return False, ("life_span is out of range "
                           f"{cls.MIN_LIFE_SPAN} - {cls.MAX_LIFE_SPAN}.")

        return True, ''

    @staticmethod
    def delete_expired():
        """ Method to run periodically
        to remove expired urls and keep database clear.
        """
        print('SCHEDULE CLEAR TASK')


class IdCreator:

    # Use base-62 with safe characters.
    _SYMBOLS = py_string.ascii_letters + py_string.digits

    @classmethod
    def encode(cls, num: int) -> str:
        """Encode a positive number into 62 Base and return the string."""
        if num == 0:
            return cls._SYMBOLS[0]
        arr = []
        arr_append = arr.append  # Extract bound-method for faster access.
        _divmod = divmod  # Access to locals is faster.
        base = len(cls._SYMBOLS)
        while num:
            num, rem = _divmod(num, base)
            arr_append(cls._SYMBOLS[rem])
        arr.reverse()
        return ''.join(arr)

    @classmethod
    def decode(cls, string: str) -> Optional[int]:
        """Decode a Base 62 encoded string into the number."""
        if not cls.validate_input(string):
            return None

        base = len(cls._SYMBOLS)
        strlen = len(string)
        num = 0

        idx = 0
        for char in string:
            power = (strlen - (idx + 1))
            num += cls._SYMBOLS.index(char) * (base ** power)
            idx += 1

        return num

    @classmethod
    def validate_input(cls, string: str) -> bool:
        """String validation before decode."""
        if not string or any(x not in cls._SYMBOLS for x in string):
            return False
        return True
