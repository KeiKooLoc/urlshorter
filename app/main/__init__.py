from flask import Blueprint

main = Blueprint('main', __name__)


from . import views, errors
api_docs_view = views.ApiDocsView.as_view('api_docs')
main.add_url_rule('/docs/', view_func=api_docs_view)
main.add_url_rule('/docs/<doc_name>', view_func=api_docs_view)
main.add_url_rule('/', view_func=views.UrlView.as_view('main'))
main.add_url_rule('/home', view_func=views.UrlView.as_view('home'))
main.add_url_rule('/<code>', view_func=views.RedirectView.as_view('redirect'))
