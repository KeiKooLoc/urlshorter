# External
from flask import (render_template, redirect, url_for,
                   current_app, abort, request, session)
from flask_sqlalchemy import get_debug_queries
from flask.views import MethodView, View
# Internal
from . import main
from .forms import LinkForm
from ..models import Url
from ..core import IdCreator


class UrlView(MethodView):

    template = 'home.html'

    def get(self):
        new = session.get('new_link')
        original = session.get('original_link')
        if new and original:
            session.clear()
            return self.render_template(new=new, original=original)
        return self.render_template()

    def post(self):
        form = self.form()
        if form.validate_on_submit():
            url = Url.create_link(original=form.url.data,
                                  life_span=form.life_span.data)
            session['new_link'] = url.new_url()
            session['original_link'] = url.original
            return redirect(url_for('.home'))
        else:
            return self.render_template(form=form)

    @classmethod
    def render_template(cls, form=None, **kwargs):
        if not form:
            form = cls.form()
        return render_template(cls.template, form=form, **kwargs)

    @staticmethod
    def form():
        return LinkForm()


class ApiDocsView(View):
    methods = ['GET']

    _doc_templates = {
        'main': 'docs/main.html',
        'short': 'docs/short.html'
    }

    def dispatch_request(self, *args, **kwargs):
        doc_name = request.view_args.get('doc_name', 'main')
        template = self._doc_templates.get(doc_name)
        if not template:
            abort(404)
        api_host = request.url_root + 'api'
        return render_template(template, api_host=api_host)


class RedirectView(View):
    methods = ['GET']

    def dispatch_request(self, code=None):
        code = request.view_args.get('code')
        print('REQUEST', code)
        url_id = IdCreator.decode(code)
        url = Url.query.get(url_id)
        if not url_id or not url or url.is_expired():
            abort(404)
        else:
            return redirect(url.original)


@main.after_app_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config['SLOW_DB_QUERY_TIME']:
            current_app.logger.warning(f'Slow query: {query.statement}'
                                       f'\nParameters: {query.parameters}'
                                       f'\nDuration: {query.duration}'
                                       f'\nContext: {query.context}\n')
    return response


@main.route('/shutdown')
def server_shutdown():
    if not current_app.testing:
        abort(404)
    shutdown = request.environ.get('werkzeug.server.shutdown')
    if not shutdown:
        abort(500)
    shutdown()
    return 'Shutting down...'
