# External
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, IntegerField
from wtforms.validators import DataRequired, ValidationError
from wtforms.widgets import html5
# Internal
from ..core import LinkHandler


class LinkForm(FlaskForm):

    url = StringField('Link',
                      validators=[DataRequired()],
                      render_kw={"placeholder": "Paste your link"})

    life_span = IntegerField('Expires in (days)',
                             validators=[DataRequired()],
                             widget=html5.NumberInput(
                                 min=LinkHandler.MIN_LIFE_SPAN,
                                 max=LinkHandler.MAX_LIFE_SPAN),
                             default=LinkHandler.DEFAULT_LIFE_SPAN)
    submit = SubmitField('Generate')

    def validate_url(self, field):
        status, message = LinkHandler.validate_url(field.data)
        if not status:
            raise ValidationError(message)
