# External
from flask_restful import Resource, marshal_with
from flask_restful.fields import Boolean, String
from flask_restful import reqparse
# Internal
from ..models import Url
from ..core import LinkHandler


_response = {
    'ok': Boolean,
    'new': String,
    'original': String,
    'expired_in': String
}


class UrlShorter(Resource):

    @marshal_with(_response)
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("url",
                            type=self._url,
                            location="json",
                            required=True)
        parser.add_argument("life_span",
                            type=self._life_span,
                            location="json",
                            default=LinkHandler.DEFAULT_LIFE_SPAN)
        args = parser.parse_args(strict=True)
        url = Url.create_link(original=args['url'],
                              life_span=args['life_span'])
        response = {'ok': True}
        response.update(url.to_json())
        return response, 201

    @staticmethod
    def _url(value: str, name: str):
        status, message = LinkHandler.validate_url(value)
        if not status:
            raise ValueError(message)
        return value

    @staticmethod
    def _life_span(value: int, name: str):
        status, message = LinkHandler.validate_life_span(value)
        if not status:
            raise ValueError(message)
        return value
