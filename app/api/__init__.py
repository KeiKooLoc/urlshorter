from flask import Blueprint
from flask_restful import Api

api_blueprint = Blueprint('api', __name__)
api = Api(api_blueprint, catch_all_404s=True)


from . import resources

api.add_resource(resources.UrlShorter, '/short')
