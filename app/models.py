import datetime
# External
from flask import request
# Internal
from app import db
from app.core import IdCreator


class Url(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    original = db.Column(db.Text, nullable=False)
    expired_in = db.Column(db.DateTime, nullable=False)
    created = db.Column(db.DateTime, default=datetime.datetime.utcnow)

    def __repr__(self):
        return f'<URL {self.original}>'

    def to_json(self) -> dict:
        return {
            'new': self.new_url(),
            'original': self.original,
            'expired_in': str(self.expired_in)
        }

    @classmethod
    def create_link(cls, original: str, life_span: int):
        url = cls(original=original)
        url.set_expired_date(life_span)
        db.session.add(url)
        db.session.commit()
        return url

    def set_expired_date(self, days: int):
        timestamp = datetime.datetime.utcnow() + datetime.timedelta(days=days)
        self.expired_in = timestamp

    def new_url(self) -> str:
        return request.url_root + self.get_code()

    def get_code(self) -> str:
        """ Get record id encoded in 62-base."""
        return IdCreator.encode(self.id)

    def is_expired(self, delete: bool = True) -> bool:
        """ Check if the link has expired.
        And delete record if 'delete' parameter set to True.
        """
        expired = datetime.datetime.utcnow() > self.expired_in
        if delete and expired:
            db.session.delete(self)
            db.session.commit()
        return expired
