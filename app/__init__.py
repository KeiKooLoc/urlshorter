import atexit
# External
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from apscheduler.schedulers.background import BackgroundScheduler
# Internal
from config import config


db = SQLAlchemy()
cors = CORS()

scheduler = BackgroundScheduler()


def create_app(config_name: str, expired_checker: bool = True):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    cors.init_app(app)
    db.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .api import api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api')

    # If app created with expired checker
    # - start periodical process to delete expired urls.
    # if expired_checker:
    #     from .core import LinkHandler
    #     scheduler.add_job(func=LinkHandler.delete_expired,
    #                       trigger="interval",
    #                       days=1)
    #     scheduler.start()
    #     # Shut down the scheduler when exiting the app.
    #     atexit.register(lambda: scheduler.shutdown())

    return app
