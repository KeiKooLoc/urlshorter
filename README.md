# URL Shorter

Hosted App: https://calm-fortress-14448.herokuapp.com/

Diagrams: https://drive.google.com/file/d/1LmLbRhoB7QySWXX5Y3BQS76cDoaJC43a/view?usp=sharing

App developed on python 3.7.


**To start application:**

```$ export FLASK_APP=manage.py ```

```$ flask run```

**To run API tests:**

```$ python -m unittest tests.test_api```
